'use strict'

import { Sequelize } from 'sequelize'

// *============================
// *        SEQUELIZE
// *============================
const sequelize = new Sequelize('ecommerce', 'root', 'pass', {
  host: 'db',
  dialect: 'mysql',
  define: {
    timestamps: false
  },
  logging: process.env.DEBUG ? (...msg) => console.log(msg) : console.log
})

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch((err) => {
    console.log('Unable to connect to the database:', err)
  })

export default sequelize
