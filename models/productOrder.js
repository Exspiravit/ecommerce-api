'use strict'

import { DataTypes, Model } from 'sequelize'
import sequelize from './sequelize'

import Product from './product'
import Order from './order'

class ProductOrder extends Model {}

ProductOrder.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  quantity: {
    type: DataTypes.INTEGER
  },
  unitPrice: {
    type: DataTypes.FLOAT
  },
  partialPrice: {
    type: DataTypes.FLOAT
  },
  idOrder: {
    type: DataTypes.INTEGER
  },
  idProduct: {
    type: DataTypes.INTEGER
  }
}, {
  sequelize,
  modelName: 'ProductOrder'
})

Product.belongsToMany(Order, { through: ProductOrder })
Order.belongsToMany(Product, { through: ProductOrder })

export default ProductOrder
