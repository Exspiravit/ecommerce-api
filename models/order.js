'use strict'

import { DataTypes, Model } from 'sequelize'
import sequelize from './sequelize'

class Order extends Model {}

Order.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  code: {
    type: DataTypes.STRING(5)
  },
  totalNet: {
    type: DataTypes.FLOAT
  },
  shippingPrice: {
    type: DataTypes.FLOAT
  },
  taxesPrice: {
    type: DataTypes.FLOAT
  },
  total: {
    type: DataTypes.FLOAT
  },
  dateSend: {
    type: DataTypes.DATEONLY
  }
},
{
  sequelize,
  modelName: 'Order'
})

export default Order
