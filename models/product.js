'use strict'

import { DataTypes, Model } from 'sequelize'
import sequelize from './sequelize'

class Product extends Model {}

Product.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING
  },
  price: {
    type: DataTypes.FLOAT
  },
  imgUrl: {
    type: DataTypes.STRING
  }
}, {
  sequelize,
  modelName: 'Product'
})

export default Product
