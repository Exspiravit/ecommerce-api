'use strict'

import { Product } from '../../models'
import moment from 'moment'

export async function calculateCost (products) {
  const res = {
    product: 0,
    shippingCost: 0,
    taxes: 0,
    total: 0
  }

  for (let i = 0; i < products.length; i++) {
    const p = await Product.findByPk(products[i].product)
    res.product += p.price * products[i].quantiy
  }
  res.taxes = res.product * process.env.TAXES
  res.shippingCost = res.product * process.env.SHIPPINGCOST
  res.total = res.product + res.shippingCost
  res.product -= res.taxes
  console.log(res)

  return res
}

export function calculateSend () {
  const toDay = moment().isoWeekday()
  return [5, 6, 7].indexOf(toDay) !== -1
    ? moment().add(8 - toDay, 'days').format('yyyy-MM-DD')
    : moment().add(1, 'days').format('yyyy-MM-DD')
}
