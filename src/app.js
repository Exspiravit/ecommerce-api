'use strict'

import express from 'express'
import cors from 'cors'
import expressGraphql from 'express-graphql'

import schema from './graphql/schemas/schema'

// import bodyParser from 'body-parser'
require('dotenv').config()

const app = express()

app.use(cors())

// console.log(schema.toString())

// *============================
// *        GRAPHQL
// *============================
app.use('/graphql', expressGraphql({
  schema: schema,
  graphiql: true
}))

app.listen(process.env.PORT, () =>
  console.log(`Listen on port ${process.env.PORT}`)
)
