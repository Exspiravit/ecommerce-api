import {
  GraphQLNonNull as NonNull,
  GraphQLString as String,
  GraphQLFloat as Float
} from 'graphql'
import sequelize from '../../../models/sequelize'

import { Product } from '../../../models'
import ProductType from '../types/product'

export default {

  addProduct: {
    type: ProductType,
    args: {
      name: {
        type: new NonNull(String)
      },
      price: {
        type: new NonNull(Float)
      },
      imgUrl: {
        type: new NonNull(String)
      }
    },
    resolve: async (_, { name, price, imgUrl }) => {
      return sequelize.transaction(async t => {
        return Product.create({
          name,
          price,
          imgUrl
        }, { transaction: t }
        )
      })
    }
  }
}
