import {
  GraphQLList as List,
  GraphQLObjectType as ObjectType,
  GraphQLFloat as Float
} from 'graphql'
import sequelize from '../../../models/sequelize'
import GraphQLJSON from 'graphql-type-json'

import { Order, ProductOrder, Product } from '../../../models'
import OrderType from '../types/order'
import ProductInputType from '../types/productInput'

import { calculateCost, calculateSend } from '../../utils/util'

const preCalcCard = new ObjectType({
  name: 'preCalcCard',
  fields: () => ({
    products: { type: Float },
    shippingCost: { type: Float },
    taxes: { type: Float },
    total: { type: Float }
  })
})

export default {

  addOrder: {
    type: OrderType,
    args: {
      products: {
        type: new List(ProductInputType)
      }
    },
    resolve: async (_, { products }) => {
      const o = await Order.findOne({
        order: [
          ['id', 'DESC']
        ]
      })
      let code
      if (o) {
        code = o.code = (Number(o.code.substr('1')) + 1).toString()
        code = `P${'0'.repeat(4 - o.code.length)}${o.code}`
      } else {
        code = 'P0001'
      }
      const { product, shippingCost, taxes, total } = await calculateCost(products)
      const order = await sequelize.transaction(async t => {
        return Order.create({
          code,
          totalNet: product.toFixed(2),
          shippingPrice: shippingCost.toFixed(2),
          taxesPrice: taxes.toFixed(2),
          total: total.toFixed(2),
          dateSend: calculateSend()
        }, { transaction: t }
        )
      })
      await products.map(async (p) => {
        const productF = await Product.findByPk(p.product, {
          raw: true
        })
        await sequelize.transaction(async t => {
          return ProductOrder.create({
            quantity: p.quantiy,
            unitPrice: productF.price,
            partialPrice: productF.price * p.quantiy,
            idOrder: order.id,
            idProduct: productF.id
          }, { transaction: t })
        })
      })

      return order
    }
  },
  preCalcCard: {
    type: preCalcCard,
    args: {
      products: {
        type: List(ProductInputType)
      }
    },
    resolve: async (_, { products }) => {
      return calculateCost(products)
    }
  },
  calculateDate: {
    type: GraphQLJSON,
    resolve: () => {
      return calculateSend()
    }
  }
}
