import { GraphQLObjectType as ObjectType } from 'graphql'

import Order from './order'
import Product from './product'

const Mutation = new ObjectType({
  name: 'RootMutation',
  description: '🐈 Toaster Mutation',
  fields: Object.assign({},
    Order,
    Product
  )
})

export default Mutation
