import {
  GraphQLInt as Int,
  GraphQLNonNull as NonNull,
  GraphQLString as String,
  GraphQLList as List
} from 'graphql'

import { Op } from 'sequelize'

import { Product } from '../../../models'
import ProductType from '../types/product'

export default {
  productAll: {
    type: new List(ProductType),
    args: {},
    resolve: async (_) => {
      return Product.findAll()
    }
  },
  productById: {
    type: ProductType,
    args: {
      id: {
        type: new NonNull(Int)
      }
    },
    resolve: async (_, { id }) => {
      return Product.findByPk(id)
    }
  },
  productByName: {
    type: new List(ProductType),
    args: {
      name: {
        type: String
      }
    },
    resolve: async (_, { name }) => {
      return Product.findAll({
        where: {
          name: {
            [Op.like]: [`%${name}%`]
          }
        }
      })
    }
  }
}
