import {
  GraphQLInt as Int,
  GraphQLNonNull as NonNull,
  GraphQLString as String,
  GraphQLList as List
} from 'graphql'

import { Order } from '../../../models'
import OrderType from '../types/order'

export default {
  OrderAll: {
    type: new List(OrderType),
    resolve: async () => {
      return Order.findAll()
    }
  },
  OrderById: {
    type: OrderType,
    args: {
      id: {
        type: new NonNull(Int)
      }
    },
    resolve: async (_, { id }) => {
      return Order.findByPk(id)
    }
  },
  OrderByCode: {
    type: new List(OrderType),
    args: {
      code: {
        type: String
      }
    },
    resolve: async (_, { code }) => {
      return Order.findOne({
        where: {
          code: code
        }
      })
    }
  }
}
