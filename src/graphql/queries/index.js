import { GraphQLObjectType as ObjectType } from 'graphql'

import Order from './order'
import Product from './product'

const Query = new ObjectType({
  name: 'RootQuery',
  description: '🐈 Toaster Queries',
  fields: Object.assign(
    {},
    Order,
    Product
  )
})

export default Query
