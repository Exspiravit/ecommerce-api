import {
  GraphQLInt as Int,
  GraphQLInputObjectType as InputObjectType,
  GraphQLFloat as Float
} from 'graphql'

export default new InputObjectType({
  name: 'ProductInput',
  fields: () => ({
    product: { type: Int },
    quantiy: { type: Float }
  })
})
