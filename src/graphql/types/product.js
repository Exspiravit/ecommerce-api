import { GraphQLInt as Int, GraphQLNonNull as NonNull, GraphQLObjectType as ObjectType, GraphQLString as String, GraphQLFloat as Float } from 'graphql'

export default new ObjectType({
  name: 'Product',
  fields: () => ({
    id: { type: new NonNull(Int) },
    name: { type: String },
    price: { type: Float },
    imgUrl: { type: String }
  })
})
