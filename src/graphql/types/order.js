import {
  GraphQLInt as Int,
  GraphQLNonNull as NonNull,
  GraphQLObjectType as ObjectType,
  GraphQLString as gqlString,
  GraphQLFloat as Float,
  GraphQLList as List
} from 'graphql'
import { Order, Product, ProductOrder } from '../../../models'
import ProductOrdersType from './productOrder'

Order.Products = Order.belongsToMany(Product, {
  through: 'ProductOrders',
  foreignKey: 'idOrder',
  otherKey: 'idProduct'
})

export default new ObjectType({
  name: 'Order',
  fields: () => ({
    id: { type: new NonNull(Int) },
    code: { type: gqlString },
    totalNet: { type: Float },
    shippingPrice: { type: Float },
    taxesPrice: { type: Float },
    total: { type: Float },
    dateSend: { type: gqlString }
    // *    Esta propiedad serviria para hacer reportes mucho mas rapidos
    // TODO FALTA TESTEAR
    // Products: {
    //   type: new List(ProductOrdersType),
    //   resolve: async (obj) => {
    //     const p = await ProductOrder.findAll({
    //       attributes: [
    //         ['id', 'id'],
    //         ['quantity', 'quantity'],
    //         ['unitPrice', 'unitPrice'],
    //         ['partialPrice', 'partialPrice'],
    //         ['idOrder', 'idOrder'],
    //         ['idProduct', 'idProduct']
    //       ],
    //       where: {
    //         idOrder: obj.id
    //       },
    //       raw: true
    //     })
    //     console.log('****************')
    //     console.log(p.map(p => {
    //       return {
    //         id: p.id,
    //         quantity: p.quantity,
    //         unitPrice: p.unitPrice,
    //         partialPrice: p.partialPrice,
    //         idOrder: p.idOrder,
    //         idProduct: p.idProduct
    //       }
    //     }))
    //     const j = p.map(p => {
    //       return {
    //         id: p.id,
    //         quantity: p.quantity,
    //         unitPrice: p.unitPrice,
    //         partialPrice: p.partialPrice,
    //         idOrder: p.idOrder,
    //         idProduct: p.idProduct
    //       }
    //     })
    //     console.log(j)
    //     console.log('****************')
    //     return j
    //     // return [{
    //     //   id: 37,
    //     //   quantity: 2,
    //     //   unitPrice: 2,
    //     //   partialPrice: 4,
    //     //   idOrder: 37,
    //     //   idProduct: 1
    //     // },
    //     // {
    //     //   id: 38,
    //     //   quantity: 1,
    //     //   unitPrice: 2,
    //     //   partialPrice: 2,
    //     //   idOrder: 37,
    //     //   idProduct: 2
    //     // }]
    //   }
    // }
  })
})
