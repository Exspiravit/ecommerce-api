import { GraphQLInt as Int, GraphQLNonNull as NonNull, GraphQLObjectType as ObjectType, GraphQLFloat as Float } from 'graphql'

export default new ObjectType({
  name: 'ProductOrder',
  fields: () => ({
    id: { type: new NonNull(Int) },
    quantity: { type: Int },
    unitPrice: { type: Float },
    partialPrice: { type: Float },
    idOrder: { type: Int },
    idProduct: { type: Int }
  })
})
